# Glossy test application

| **Author** |**Last modified**|
|:---:|:---:|
|Romain Jacob  |20-08-2019|

## Summary
Simple Glossy application used to show the importance of the time at which a 
protocol runs (eg day vs night). 
Ultimately shows that N=1 may (appear to) be more reliable than N=2, which is
an experiment artifact: N=1 performs better only because tests run during
"more favorable" times than N=2.

N: number of retransmissions for each node in a Glossy flood  
The larger N, the more reliable the flood (at the cost of latency and energy). 

## Compilation command
|Platform| Compilation command |
|:---|:---|
|TelosB | make TARGET=sky |
|DPP-cc430 | make TARGET=dpp |

