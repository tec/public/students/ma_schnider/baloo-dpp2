The `connectivity_matrix.py` script computes a connectivity map of testbed setups using the `gmw-get-connectivity` application.

Usage: 
- Set the parameters for `gmw-get-connectivity` in the Makefile and recompile
- Don't forget the FLOCKLAB or GRAZ flag (set in the Makefile)!
- Run on the corresponding testbed
- Download the archive file with the test results
- Run `./connectivity_matrix.py <path-to-archive>`
- Do something useful with the info :-)


The `../scripts/create-graph.py` script computes a visual connectivity map of testbed setups using the `gmw-get-connectivity` application with variable threshold.

Usage:
- as above: run test and download results
- extract results
- run `../scripts/create-graph.py <path-folder> <threshold> {graz,flocklab}
- this needs graphviz to be installed
