#!/usr/bin/env python

import sys, os, getopt
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import zipfile
import tarfile

# for bash script
import subprocess

from struct import *

def usage():
  print "Usage: connectivity_matrix.py <test-zip>"
  print "  <test-zip>: path to the archive file containing test results"

##############################################################################
#
# Main
#
##############################################################################
def main(argv):

  serialfile = None
  archivefile = None

  if len(argv) < 1:
    usage()
    sys.exit()  

  archivefile = argv[0]

  if (archivefile.endswith("tar.gz")):
    tar = tarfile.open(archivefile, "r:gz")
    tar.extractall()
    tar.close()
    archivefile = archivefile.split('.')[-3]
    archivefile = archivefile.split('_')[-1]
    print 'Flocklab test'
    
    # load the corresponding node list
    nodes = [1, 2, 3, 4, 6, 7, 8,10,11,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,32,33]

  elif (archivefile.endswith("zip")):
    zip = zipfile.ZipFile(archivefile, 'r')
    archivefile = archivefile.split('/')[-1]
    archivefile = archivefile.split('.')[0]
    archivefile = archivefile.split('_')[1]
    zip.extractall('./'+ archivefile)
    zip.close()
    print 'Graz test'

    # load the corresponding node list
    nodes = [100,  104,  108,  112,  116,  150,  200,  204,  208,  212,  216,  220,  224,
101,  105,  109,  113,  117,  151,  201,  205,  209,  213,  217,  221,  225,
102,  106,  110,  114,  118,  152,  202,  206,  210,  214,  218,  222,  226,
103,  107,  111,  115,  119,  153,  203,  207,  211,  215,  219,  223]

    # generate the serial.csv file
    subprocess.call(['./logs2flocklab.sh', archivefile])

  else:
    print "Test file should be either *.zip (Graz test) or *.tar.gz (Flocklab test). Abort."
    return

  # load serial file 
  serialfile = archivefile + '/serial.csv'
  inf = open(serialfile, "r")

  # parse the serial log and fill the nodes_n_rx structure
  nodes_n_rx={}
  #print round_counter
  line = inf.readline()
  while line != '':
    if line[0]=='#':
      line = inf.readline()
      continue
    try:
      (ts, obs, id, dir, pkt) = line.split(',', 4)
    except ValueError:
      print line
      return
    try: 
      id = nodes.index(int(id))
    except ValueError:
      print line
      return

    #print id
    if "N_rx:" in pkt:
      (tag, pkt) = pkt[0:-1].split('N_rx:')
      data = pkt.split(',')
      if len(data) == len(nodes):
        for i in range(0, len(data)):
          if ':' in data[i] :
            data[i] = '10'
          try: 
            data[i] = int(data[i])
          except ValueError:
            print data, ts

        if not id in nodes_n_rx:
          nodes_n_rx[id] = []
        nodes_n_rx[id].append(data)
        #print nodes_n_rx[id]

    line = inf.readline()

  # compute the connectivity matrix  
  connectivity_matrix=[]
  for node in range(0, len(nodes)):
    size = np.shape(nodes_n_rx[node])
    data = (np.sum(nodes_n_rx[node], axis=0)).astype(float)
    data = data/(10*size[0]) # there are 10 strobes sent per round
    connectivity_matrix.append(data)
 
  connectivity_matrix = np.array(connectivity_matrix)
    
  #plot the connectivity map
  fig, ax = plt.subplots()
  im = ax.imshow(connectivity_matrix)

  node_labels = []
  for i in range(0, len(nodes)):
    node_labels.append(str(nodes[i]))


  # We want to show all ticks...
  ax.set_xticks(np.arange(len(node_labels)))
  ax.set_yticks(np.arange(len(node_labels)))
  # ... and label them with the respective list entries
  ax.set_xticklabels(node_labels)
  ax.set_yticklabels(node_labels)

  # Let the horizontal axes labeling appear on top.
  ax.tick_params(top=True, bottom=False,
                 labeltop=True, labelbottom=False)

  # Rotate the tick labels and set their alignment.
  plt.setp(ax.get_xticklabels(), ha="right",
           rotation_mode="anchor")

  # Loop over data dimensions and create text annotations.
  for i in range(len(node_labels)):
      for j in range(len(node_labels)):
          text = ax.text(j, i, round(connectivity_matrix[i, j],1),
                         ha="center", va="center", color="w")
  fig.tight_layout()
  plt.show()

if __name__ == "__main__":
  main(sys.argv[1:])
