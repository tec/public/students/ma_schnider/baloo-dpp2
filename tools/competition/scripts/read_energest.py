#!/usr/bin/env python3
import sys
import os
from read_flocklab_messages import read_messages_from_serial_csv
import re

pattern_energest = re.compile("^([0-9]+), ([0-9]+), ([0-9]+), retrans: ([0-9]+)$")

if len(sys.argv) != 2:
    print("Usage: read_energest.py <serial_file>")
    exit()

messages = read_messages_from_serial_csv(sys.argv[1])

vals = {}
num = 0
vals["retrans"] = 0
vals["cpu"] = 0
vals["listen"] = 0
vals["transmit"] = 0
missed = 0

for id, mes in messages.items():
    first = True
    for m in mes:
        splited_line = pattern_energest.match(m["message"])
        if splited_line:
            if first:
                first = False
                continue
            num += 1
            if int(splited_line.group(4)) != 0:
                vals["retrans"] = int(splited_line.group(4))
            vals["cpu"] += int(splited_line.group(1))
            vals["listen"] += int(splited_line.group(2))
            vals["transmit"] += int(splited_line.group(3))
        elif "missed something" in m["message"]:
            missed += 1

print("Results:")
print("Retransmissions: {}".format(vals["retrans"]))
print("Missed: {}".format(missed))
print("CPU: {}".format(vals["cpu"]/num))
print("listen: {}".format(vals["listen"]/num))
print("transmit: {}".format(vals["transmit"]/num))

