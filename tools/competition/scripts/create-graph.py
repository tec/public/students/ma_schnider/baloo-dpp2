#!/usr/bin/env python3
import re
import sys
import os
from collections import defaultdict
#print (sys.argv)

import argparse

parser = argparse.ArgumentParser(description='Create connectivity map from serial.csv')
parser.add_argument('in_file', metavar='f', help='path to the serial file')
parser.add_argument('threshold', metavar='th', type=float,
        help='define the threshold for connectivity')
parser.add_argument('testbed', choices=['graz','flocklab'], help='define the testbed that was used (graz/flocklab)')

args = parser.parse_args()

if args.testbed == 'flocklab':
    node_order=[1,2,3,4,6,7,8,10,11,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,32,33]
    pattern_Node_id = re.compile("^.*,([0-9]+),r,\[INFO.*$")
else:
    node_order = [100,  104,  108,  112,  116,  150,  200,  204,  208,  212,  216,  220,  224,
        101,  105,  109,  113,  117,  151,  201,  205,  209,  213,  217,  221,  225,
        102,  106,  110,  114,  118,  152,  202,  206,  210,  214,  218,  222,  226,
        103,  107,  111,  115,  119,  153,  203,  207,  211,  215,  219,  223]
    pattern_Node_id = re.compile("^.*Node id: *([0-9]+).*$")


pattern_Node_con = re.compile("^.*N_rx:(.*)$")
pattern_Node_matrix = re.compile("([0-9:])")

nodes = defaultdict(lambda: defaultdict(int))
numEntries = defaultdict(int)

folder = args.in_file
for filename in os.listdir(folder):
    if filename.endswith(".txt") or filename.endswith(".csv"):
        with open(folder + "/" + filename, "r") as in_file:
            fid = 0;
            for line in in_file:
                splited_line = pattern_Node_id.match(line)
                if splited_line:
                    fid = int(splited_line.group(1))
                splited_line = pattern_Node_con.match(line)
                if splited_line:
                    matches = pattern_Node_matrix.findall(splited_line.group(1))
                    # the first match is from the N_rx:
                    i=0
                    if fid == 0:
                        continue
                    for match in matches:
                        if match == ':':
                            match = 10
                        nodes[fid][node_order[i]] += int(match)
                        i+=1
                    numEntries[fid] += 1

res = {}
for fid,node in nodes.items():
    res[fid] = dict((k, v/numEntries[fid]) for k, v in node.items())
#import pdb; pdb.set_trace()

with open("/tmp/graph_gen", "w") as out_file:
    out_file.write("graph {\n")
    out_file.write("    overlap = scale;\n")
    out_file.write("    splines = true;\n")
    for fid,node in res.items():
        out_file.write('    {}\n'.format(fid))
        for con in node_order:
            if fid not in node_order:
                print("new id: {}".format(fid))
            if (con in res and con in node and fid in res[con] and (node[con]+res[con][fid])/2 >= args.threshold):
                out_file.write('    {}--{}[penwidth={}, len=5]\n'.format(fid, con, ((node[con]+res[con][fid])/2)-args.threshold))
            elif con in node and node[con] >= args.threshold:
                out_file.write('    {}--{}[penwidth={}, len=5]\n'.format(fid, con, (node[con])-args.threshold))
    out_file.write("}")

print("wrote output to graph.pdf")

from subprocess import call
call(["neato", "-Tpdf", "/tmp/graph_gen" ,"-o./graph.pdf"])
call(["open", "./graph.pdf"])
