#!/usr/bin/env python3
import re
import os
from collections import defaultdict

pattern_Node_message = re.compile("^([0-9]+.[0-9]+),([0-9]+),[0-9]+,r,\[ *([0-9]+)\] (.*)$")
# 0: whole match
# 1: absolute time
# 2: id
# 3: relative time
# 4: message


def read_messages_from_serial_csv(csv_file):
    if not os.path.isfile(csv_file):
        raise FileNotFoundError("The file '{}' does not exists".format(csv_file))

    messages = defaultdict(list)
    with open(csv_file, "r") as in_file:
        for line in in_file:
            splited_line = pattern_Node_message.match(line)
            if splited_line:
                message = {}
                message["abstime"] = splited_line.group(1)
                message["reltime"] = splited_line.group(3)
                message["message"] = splited_line.group(4)
                messages[splited_line.group(2)].append(message)

    return dict(messages)
