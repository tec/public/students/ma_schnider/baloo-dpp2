#!/usr/bin/env python3
import sys
import os
from read_flocklab_messages import read_messages_from_serial_csv

if len(sys.argv) != 4:
    print("Usage: get_active_part_of_network.py <serial_file> <time to look at> <period/time range>")
    exit()

messages = read_messages_from_serial_csv(sys.argv[1])
time = int(sys.argv[2])
period = int(sys.argv[3])

script_dir = os.path.dirname(__file__)
rel_path = "../flocklab.dot"
flocklab_graph = os.path.join(script_dir, rel_path)

ids = []

for id, mes in messages.items():
    for m in mes:
        if (int(m["reltime"]) <= time and int(m["reltime"]) > time-period) and "I'm in range" in m["message"]:
            ids.append(" {} ".format(id))
            break

from shutil import copy

copy(flocklab_graph, "/tmp/graph_gen")
import fileinput
with fileinput.FileInput("/tmp/graph_gen", inplace=True, backup='.bak') as out_file:
    for line in out_file:
        if any(i in line for i in ids):
            print(line.replace("]",", bgcolor=green, style=filled]"),end='')
        else:
            print(line)

from subprocess import call
call(["neato", "-n", "-s", "-Tpdf", "/tmp/graph_gen" ,"-o./actives.pdf"])
call(["open", "./actives.pdf"])
