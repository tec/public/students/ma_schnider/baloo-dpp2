/*
 * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Jonas Baechli
 */
/**
 * @file    Configuration for the GMW-based eLWB.
 */

#ifndef GMW_ELWB_CONF_H_
#define GMW_ELWB_CONF_H_

#include "contiki.h"

#ifndef GMW_ELWB_PERIOD
/* the idle period in ms */
#define GMW_ELWB_PERIOD     5000
#endif /* GMW_ELWB_PERIOD */

#ifndef GMW_ELWB_T_S_EC
/* the period for the second schedule of the event contention round in ms */
#define GMW_ELWB_T_S_EC     50
#endif /* GMW_ELWB_T_S_EC */

#ifndef GMW_ELWB_T_EVENT
/* the period for the event round in ms (from start of first round to event round) */
#define GMW_ELWB_T_EVENT    100
#endif /* GMW_ELWB_T_EVENT */

#ifndef GMW_ELWB_T_DATA
/* the period for the data round in ms (from start of event round to data round) */
#define GMW_ELWB_T_DATA     200
#endif /* GMW_ELWB_T_DATA */

#ifndef GMW_ELWB_CONF_USE_STATIC_NODE_LIST
/* if GMW_ELWB_CONF_USE_STATIC_NODE_LIST is 1, the content of
 * GMW_ELWB_CONF_STATIC_NODE_LIST is used instead of the ack discovery process */
#define GMW_ELWB_CONF_USE_STATIC_NODE_LIST   0

#ifndef GMW_ELWB_CONF_STATIC_NODE_LIST_SIZE
#error "GMW_ELWB_CONF_STATIC_NODE_LIST_SIZE undefined but GMW_ELWB_CONF_USE_STATIC_NODE_LIST defined"
#endif /* GMW_ELWB_CONF_STATIC_NODE_LIST_SIZE */

#ifndef GMW_ELWB_CONF_STATIC_NODE_LIST
#error "GMW_ELWB_CONF_USE_STATIC_NODE_LIST undefined but GMW_ELWB_CONF_USE_STATIC_NODE_LIST defined"
#endif /* GMW_ELWB_CONF_USE_STATIC_NODE_LIST */
#endif /* GMW_ELWB_CONF_USE_STATIC_NODE_LIST */

#ifndef GMW_GMW_ELWB_CONF_IN_BUFFER_SIZE
/* size (#elements) of the internal data buffer/queue for incoming messages,
 * should be at least GMW_ELWB_CONF_MAX_DATA_SLOTS */
#define GMW_ELWB_CONF_IN_BUFFER_SIZE         GMW_ELWB_CONF_MAX_DATA_SLOTS
#endif /* GMW_ELWB_CONF_IN_BUFFER_SIZE */

#ifndef GMW_ELWB_CONF_OUT_BUFFER_SIZE
/* size (#elements) of the internal data buffer/queue for outgoing messages */
#define GMW_ELWB_CONF_OUT_BUFFER_SIZE        3
#endif /* GMW_ELWB_CONF_IN_BUFFER_SIZE */

#endif /* GMW_ELWB_CONF_H_ */
