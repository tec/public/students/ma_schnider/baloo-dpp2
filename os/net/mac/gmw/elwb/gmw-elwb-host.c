/*
 * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Jonas Baechli
 */
/*---------------------------------------------------------------------------*/
#include "gmw-elwb-host.h"
#include "contiki.h"
#include "gmw-elwb.h"
#include "gmw-elwb-scheduler.h"

#include "debug-print.h"
#include "gpio.h"
#include "memb.h"
#include "list.h"
#include "node-id.h"
/*---------------------------------------------------------------------------*/
typedef struct __attribute__((packed)) elwb_data_packet {
    struct elwb_data_packet* next;
    uint16_t packet_src_id;
    uint8_t len;
    uint8_t data[GMW_ELWB_CONF_MAX_DATA_PKT_LEN+2];
} elwb_data_packet_t;
/*---------------------------------------------------------------------------*/
static gmw_sync_state_t on_control_slot_post(gmw_control_t* in_out_control,
                                             gmw_sync_event_t event);
static void on_slot_pre(uint8_t slot_index, uint16_t slot_assignee,
              uint8_t* out_len, uint8_t* out_glossy_payload,
              uint8_t is_initiator, uint8_t is_contention_slot);
static void on_slot_post(uint8_t slot_index, uint16_t slot_assignee,
               uint8_t len, uint8_t* glossy_payload,
               uint8_t is_initiator, uint8_t is_contention_slot);
static void on_round_finish(gmw_pre_post_processes_t* in_out_pre_post_processes);
static uint32_t on_bootstrap_timeout(void);
/*---------------------------------------------------------------------------*/
static gmw_elwb_state_t state;
static gmw_control_t control;
static struct process* pre_process;
static struct process* post_process;
MEMB(incoming_memb, elwb_data_packet_t, GMW_ELWB_CONF_MAX_DATA_SLOTS);
LIST(incoming_list);
/*---------------------------------------------------------------------------*/
void
gmw_elwb_host_init(struct gmw_protocol_impl* host_impl,
                   struct process* pre_proc,
                   struct process* post_proc)
{
  host_impl->on_bootstrap_timeout = &on_bootstrap_timeout;
  host_impl->on_control_slot_post = &on_control_slot_post;
  host_impl->on_slot_pre = &on_slot_pre;
  host_impl->on_slot_post = &on_slot_post;
  host_impl->on_round_finished = on_round_finish;

  state = FIRST_SCHEDULE;

  pre_process = pre_proc;
  post_process = post_proc;

  memb_init(&incoming_memb);
  list_init(incoming_list);

  gmw_elwb_scheduler_init(&control);
  gmw_set_new_control(&control);
}
/*---------------------------------------------------------------------------*/
gmw_elwb_state_t
gmw_elwb_host_get_state(void)
{
  return state;
}
/*---------------------------------------------------------------------------*/
uint8_t
gmw_elwb_host_rcv_pkt(uint8_t* out_data, uint16_t * const out_node_id)
{
  elwb_data_packet_t* pkt = list_pop(incoming_list);
  if (pkt != NULL) {
    uint8_t len = pkt->len;
    memcpy(out_data, pkt->data, len);
    if (out_node_id) {
      *out_node_id = pkt->packet_src_id;
    }
    memb_free(&incoming_memb, pkt);
    return len;
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
static gmw_sync_state_t
on_control_slot_post(gmw_control_t* in_out_control, gmw_sync_event_t event)
{
  return GMW_RUNNING;
}
/*---------------------------------------------------------------------------*/
static void
on_slot_pre(uint8_t slot_index, uint16_t slot_assignee, uint8_t* out_len,
            uint8_t* out_glossy_payload, uint8_t is_initiator,
            uint8_t is_contention_slot)
{
  switch (state) {
    case FIRST_SCHEDULE:
      /* do nothing */
      break;
    case SECOND_SCHEDULE:
      /* empty */
      DEBUG_PRINT_WARNING("State second schedule should not have slots (pre)");
      break;
    case EVENT_ROUND:
      /* the scheduler prepares slots for each node + cont slot for new nodes */
      /* do nothing */
      break;
    case DATA_ROUND:
      /* we only have to ACK new nodes */
      if (is_initiator) {
        *((uint16_t*) out_glossy_payload) = gmw_elwb_scheduler_get_pending_ack();
        *out_len = 2;
        DEBUG_PRINT_INFO("Sent ack to %u", *((uint16_t*) out_glossy_payload));
      }
      break;
    default:
      break;
  }
}
/*---------------------------------------------------------------------------*/
static void
on_slot_post(uint8_t slot_index, uint16_t slot_assignee, uint8_t len,
             uint8_t* glossy_payload, uint8_t is_initiator,
             uint8_t is_contention_slot)
{
  switch (state) {
    case FIRST_SCHEDULE:
      if (len) {
        DEBUG_PRINT_VERBOSE("received event contention");
        gmw_elwb_scheduler_notify_event();
      }
      break;
    case SECOND_SCHEDULE:
      /* just sent the schedule */
      DEBUG_PRINT_WARNING("State second schedule should not have slots(post)")
      break;
    case EVENT_ROUND:
      /* handle data requests & new nodes (contention slot) */
      if (len >= (sizeof(gmw_elwb_event_packet_t))) {
        gmw_elwb_scheduler_handle_event((gmw_elwb_event_packet_t*) glossy_payload);
      }
      break;
    case DATA_ROUND:
      if (len && !is_initiator) {
        elwb_data_packet_t* incoming_pkt = memb_alloc(&incoming_memb);
        if (incoming_pkt == 0) {
          DEBUG_PRINT_WARNING("Not enough memory to receive data!");
        } else {
          incoming_pkt->len = len;
          memcpy(incoming_pkt->data, glossy_payload, len);
          incoming_pkt->packet_src_id = slot_assignee;
          list_add(incoming_list, incoming_pkt);
        }
      }
      break;
    default:
      break;
  }
}
/*---------------------------------------------------------------------------*/
static void
on_round_finish(gmw_pre_post_processes_t* in_out_pre_post_processes)
{
  if(control.schedule.period > 1000) {
    in_out_pre_post_processes->post_process_current_round = post_process;
    in_out_pre_post_processes->pre_process_next_round = pre_process;
  } else {
    in_out_pre_post_processes->post_process_current_round = NULL;
    in_out_pre_post_processes->pre_process_next_round = NULL;
  }

  gmw_elwb_scheduler_update(&control, &state);
  gmw_set_new_control(&control);
}
/*---------------------------------------------------------------------------*/
static uint32_t
on_bootstrap_timeout(void)
{
  /* technically the host does not need this function */
  return 0;
}
/*---------------------------------------------------------------------------*/
