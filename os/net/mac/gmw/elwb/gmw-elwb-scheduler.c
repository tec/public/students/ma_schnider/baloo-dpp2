/*
 * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Jonas Baechli
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "gmw-elwb-scheduler.h"
#include "gmw-elwb.h"
#include "gmw-conf.h"

#include "list.h"
#include "memb.h"
#include "debug-print.h"
#include "gpio.h"
/*---------------------------------------------------------------------------*/
typedef struct gmw_elwb_stream_list {
  struct gmw_elwb_stream_list* next;
  gmw_elwb_event_packet_t evt;
} gmw_elwb_stream_list_t;
/*---------------------------------------------------------------------------*/
static void prepare_first_schedule(gmw_control_t* control);
static void add_new_node(gmw_elwb_event_packet_t* event);
/*---------------------------------------------------------------------------*/
static uint8_t is_event_pending;
static uint8_t number_of_known_nodes;
static uint16_t pending_ack;
#if GMW_ELWB_CONF_USE_STATIC_NODE_LIST
static uint16_t static_node_list[] = {GMW_ELWB_CONF_STATIC_NODE_LIST};
#endif
LIST(streams_list);
MEMB(streams_memb, gmw_elwb_stream_list_t, GMW_ELWB_CONF_MAX_N_STREAMS);
/*---------------------------------------------------------------------------*/
void
gmw_elwb_scheduler_init(struct gmw_control* control)
{
  memb_init(&streams_memb);
  list_init(streams_list);
  is_event_pending = 0;
  number_of_known_nodes = 0;

  DEBUG_PRINT_MSG_NOW("GMW elwb initialised: T_ctrl=%u T_data=%u",
                      (uint16_t)GMW_CONF_T_CONTROL, (uint16_t)GMW_CONF_T_DATA);

#if GMW_ELWB_CONF_USE_STATIC_NODE_LIST
  DEBUG_PRINT_MSG_NOW("GMW_ELWB_CONF_USE_STATIC_NODE_LIST");
  int i;
  gmw_elwb_event_packet_t event;
  uint8_t number_of_nodes = GMW_ELWB_CONF_STATIC_NODE_LIST_SIZE;
  DEBUG_PRINT_MSG_NOW("number_of_nodes = %u", number_of_nodes);
  for (i=0; i<number_of_nodes; i++) {
    if (static_node_list[i] != HOST_ID) {
      event.n_data_pkts = 0;
      event.sender_id = static_node_list[i];
      add_new_node(&event);
    }
  }

  /* reset pending ack (set in add_new_node() */
  pending_ack = 0;
#endif /* GMW_ELWB_CONF_USE_STATIC_NODE_LIST */

  gmw_control_init(control);
  control->schedule.time = 0;
  prepare_first_schedule(control);
}
/*---------------------------------------------------------------------------*/
static const char* state_to_string[4] = {"first", "second", "event", "data"};
void
gmw_elwb_scheduler_update(struct gmw_control* control, gmw_elwb_state_t* state)
{
  gmw_schedule_t* sched = &control->schedule;

  sched->time += sched->period;

  switch(*state) {
    case FIRST_SCHEDULE:
      /* preparing second schedule */
      *state = SECOND_SCHEDULE;
      if (is_event_pending) {
        sched->period = GMW_ELWB_T_EVENT - GMW_ELWB_T_S_EC;
      } else {
        sched->period = GMW_ELWB_PERIOD - GMW_ELWB_T_S_EC;
      }
      sched->n_slots = 0;
      GMW_CONTROL_CLR_CONFIG(control);
      break;
    case SECOND_SCHEDULE:
      /* prepare first schedule or event round */
      if (is_event_pending) {
        *state = EVENT_ROUND;
        sched->period = GMW_ELWB_T_DATA;
        /* assign a slot to each know node + 1 contention slot */
        sched->n_slots = number_of_known_nodes + 1;

        uint8_t i = 0;
        /* iterate through list of known nodes and assign a slot to each of them */
        gmw_elwb_stream_list_t* list_item;
        for(list_item = list_head(streams_list); list_item != NULL; list_item =
                list_item_next(list_item)) {
          sched->slot[i] = list_item->evt.sender_id;
          i++;
          if (i >= GMW_CONF_MAX_SLOTS) {
            DEBUG_PRINT_ERROR("Not enough event slots!");
            break;
          }
        }

        if (i < GMW_CONF_MAX_SLOTS) {
          sched->slot[number_of_known_nodes] = GMW_SLOT_CONTENTION;
        }
      } else {
        *state = FIRST_SCHEDULE;
        prepare_first_schedule(control);
      }
      break;
    case EVENT_ROUND:
      /* prepare data round */
      *state = DATA_ROUND;
      uint8_t i = 0, j=0, k=0;

#if !GMW_ELWB_CONF_USE_STATIC_NODE_LIST
      uint8_t has_ack = (pending_ack != 0);
      if (has_ack) {
        sched->slot[0] = HOST_ID;
        i++;
      }
#endif

      /* iterate through known nodes and assign them the number of data slots
       *  they requested at least as long as we have the space
       */
      gmw_elwb_stream_list_t* list_item;
      for(list_item = list_head(streams_list); list_item != NULL; list_item =
          list_item_next(list_item)) {
        k=0;
        for (j=0; (j<list_item->evt.n_data_pkts)&&(i<GMW_CONF_MAX_SLOTS); j++) {
          sched->slot[i] = list_item->evt.sender_id;
          i++;
          k++;
        }
        list_item->evt.n_data_pkts -= k;
      }

      sched->n_slots = i;

      sched->period = GMW_ELWB_PERIOD - GMW_ELWB_T_EVENT - GMW_ELWB_T_DATA;
      break;
    case DATA_ROUND:
      /* prepare first schedule */
      *state = FIRST_SCHEDULE;
      prepare_first_schedule(control);
      break;
    default:
      break;
  }

  /* make sure to transmit the current state as a user byte */
  control->user_bytes[0] = (uint8_t) *state;
  GMW_CONTROL_SET_USER_BYTES(control);

  DEBUG_PRINT_INFO("Sched update (%s): t=%lu, T=%u n=%u",
                   state_to_string[(uint8_t ) *state], sched->time, sched->period,
                   GMW_SCHED_N_SLOTS(sched));
}
/*---------------------------------------------------------------------------*/
void
gmw_elwb_scheduler_notify_event(void)
{
  is_event_pending = 1;
}
/*---------------------------------------------------------------------------*/
void
gmw_elwb_scheduler_handle_event(gmw_elwb_event_packet_t* event_pkt)
{
  /* n_data_pkts 0 is used for node discovery */
  if (event_pkt->n_data_pkts == 0) {
    add_new_node(event_pkt);
  } else {
    uint8_t stream_exists = 0;
    gmw_elwb_stream_list_t* list_item;
    /* iterate through list of nodes to see if it already exists */
    for(list_item = list_head(streams_list); list_item != NULL; list_item =
        list_item_next(list_item)) {
      if (list_item && (list_item->evt.sender_id == event_pkt->sender_id)) {
        list_item->evt.n_data_pkts = event_pkt->n_data_pkts;
        stream_exists = 1;
        break;
      }
    }
    if (!stream_exists) {
      DEBUG_PRINT_INFO("stream does not exist, try to add");
      add_new_node(event_pkt);
    }
  }
}
/*---------------------------------------------------------------------------*/
uint16_t
gmw_elwb_scheduler_get_pending_ack(void)
{
  uint16_t temp = pending_ack;
  pending_ack = 0;
  return temp;
}
/*---------------------------------------------------------------------------*/
static void
prepare_first_schedule(gmw_control_t* control)
{
  control->schedule.period = GMW_ELWB_T_S_EC;
  control->schedule.n_slots = 1;
  control->schedule.slot[0] = GMW_SLOT_CONTENTION;
  GMW_CONTROL_SET_CONFIG(control);
  GMW_CONTROL_SET_USER_BYTES(control);
  control->user_bytes[0] = (uint8_t) FIRST_SCHEDULE;
  is_event_pending = 0;
}
/*---------------------------------------------------------------------------*/
static void
add_new_node(gmw_elwb_event_packet_t* event)
{
  gmw_elwb_stream_list_t* prev_item = NULL;
  gmw_elwb_stream_list_t* list_item;
  for(list_item = list_head(streams_list); list_item != NULL; list_item =
      list_item_next(list_item)) {
    if (list_item->evt.sender_id == event->sender_id) {
      /* already in the list, resend ack */
      pending_ack = event->sender_id;
      return;
    } else if (list_item->evt.sender_id > event->sender_id) {
      break;
    }
    prev_item = list_item;
  }

  gmw_elwb_stream_list_t* new_item = memb_alloc(&streams_memb);
  if (new_item == NULL) {
#if GMW_ELWB_CONF_USE_STATIC_NODE_LIST
    DEBUG_PRINT_MSG_NOW("ERROR: Not enough memory for new node! %u", event->sender_id);
#else
    DEBUG_PRINT_ERROR("Not enough memory for new node! %u", event->sender_id);
#endif
  } else {
    memcpy(&new_item->evt, event, sizeof(gmw_elwb_event_packet_t));
    list_insert(streams_list, prev_item, new_item);
#if GMW_ELWB_CONF_USE_STATIC_NODE_LIST
    DEBUG_PRINT_MSG_NOW("Added %u", event->sender_id);
#else
    DEBUG_PRINT_INFO("Added %u", event->sender_id);
#endif
    pending_ack = event->sender_id;
    number_of_known_nodes++;
  }
}
/*---------------------------------------------------------------------------*/
