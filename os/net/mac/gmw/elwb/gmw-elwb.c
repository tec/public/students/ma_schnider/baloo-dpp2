/*
 * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Jonas Baechli
 */
/*---------------------------------------------------------------------------*/
#include "gmw-elwb.h"
#include "gmw-elwb-host.h"
#include "gmw-elwb-src.h"
#include "gmw.h"
#include "contiki.h"
#include "node-id.h"

#include "fifo.h"
#include "debug-print.h"
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
static gmw_protocol_impl_t host_impl;
static gmw_protocol_impl_t src_impl;
/*---------------------------------------------------------------------------*/
void
gmw_elwb_start(struct process* pre_proc, struct process *post_proc)
{
  if (node_id == HOST_ID) {
    gmw_elwb_host_init(&host_impl, pre_proc, post_proc);
  } else {
    gmw_elwb_src_init(&src_impl, pre_proc, post_proc);
  }
  gmw_start(pre_proc, post_proc, &host_impl, &src_impl);
}
/*---------------------------------------------------------------------------*/
gmw_elwb_state_t
gmw_elwb_get_state(void)
{
  if (node_id == HOST_ID) {
    return gmw_elwb_host_get_state();
  } else {
    return gmw_elwb_src_get_state();
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
elwb_send_pkt(const uint8_t * const data, uint8_t len)
{
  return gmw_elwb_src_send_pkt(data, len);
}
/*---------------------------------------------------------------------------*/
uint8_t
elwb_rcv_pkt(uint8_t* out_data, uint16_t * const out_node_id)
{
  return gmw_elwb_host_rcv_pkt(out_data, out_node_id);
}
/*---------------------------------------------------------------------------*/
