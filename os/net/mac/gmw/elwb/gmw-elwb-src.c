/*
 * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Jonas Baechli
 */

/*---------------------------------------------------------------------------*/
#include "gmw-elwb-src.h"
#include "contiki.h"
#include "gmw-elwb.h"

#include "debug-print.h"
#include "gpio.h"
/*---------------------------------------------------------------------------*/
 /* magic number to be sent in event contention slot */
#define GMW_ELWB_EVENT_MAGIC_NUMBER     (0x00)
#define DATA_BUFFER_SIZE (GMW_ELWB_CONF_OUT_BUFFER_SIZE * GMW_ELWB_CONF_MAX_DATA_PKT_LEN)
/*---------------------------------------------------------------------------*/
static gmw_sync_state_t on_control_slot_post(gmw_control_t* in_out_control,
                                             gmw_sync_event_t event);
static void on_slot_pre(uint8_t slot_index, uint16_t slot_assignee,
              uint8_t* out_len, uint8_t* out_glossy_payload,
              uint8_t is_initiator, uint8_t is_contention_slot);
static void on_slot_post(uint8_t slot_index, uint16_t slot_assignee,
               uint8_t len, uint8_t* glossy_payload,
               uint8_t is_initiator, uint8_t is_contention_slot);
static void on_round_finish(gmw_pre_post_processes_t* in_out_pre_post_processes);
static uint32_t on_bootstrap_timeout(void);
/*---------------------------------------------------------------------------*/
static gmw_elwb_state_t state;
static uint8_t is_acked_by_host;
static uint8_t data_buffer[DATA_BUFFER_SIZE];
static uint8_t data_len;
static uint8_t transmit_counter;
static uint8_t n_data_packets;
static struct process* pre_process;
static struct process* post_process;
static gmw_control_t* current_control;
/*---------------------------------------------------------------------------*/
void
gmw_elwb_src_init(struct gmw_protocol_impl* src_impl,
                  struct process* pre_proc,
                  struct process* post_proc)
{
  src_impl->on_bootstrap_timeout = &on_bootstrap_timeout;
  src_impl->on_control_slot_post = &on_control_slot_post;
  src_impl->on_slot_pre = &on_slot_pre;
  src_impl->on_slot_post = &on_slot_post;
  src_impl->on_round_finished = on_round_finish;

  data_len = 0;

  pre_process = pre_proc;
  post_process = post_proc;

  state = FIRST_SCHEDULE;

#if GMW_ELWB_CONF_USE_STATIC_NODE_LIST
  is_acked_by_host = 1;
#else
  is_acked_by_host = 0;
#endif
}
/*---------------------------------------------------------------------------*/
gmw_elwb_state_t
gmw_elwb_src_get_state(void)
{
  return state;
}
/*---------------------------------------------------------------------------*/
static gmw_sync_state_t
on_control_slot_post(gmw_control_t* in_out_control, gmw_sync_event_t event)
{
  state = in_out_control->user_bytes[0];
  current_control = in_out_control;

  return GMW_DEFAULT;
}
/*---------------------------------------------------------------------------*/
uint8_t
gmw_elwb_src_send_pkt(const uint8_t * const data, uint8_t len)
{
  if (0 != data_len) {
    DEBUG_PRINT_WARNING("Out buffer busy");
    return 0;
  } else {
    if (len > DATA_BUFFER_SIZE) {
      DEBUG_PRINT_WARNING("Out buffer too small, cutting off data");
      len = DATA_BUFFER_SIZE;
    }
    memcpy(data_buffer, data, len);
    data_len = len;
    return 1;
  }
}
/*---------------------------------------------------------------------------*/
static void
on_slot_pre(uint8_t slot_index, uint16_t slot_assignee, uint8_t* out_len,
            uint8_t* out_glossy_payload, uint8_t is_initiator,
            uint8_t is_contention_slot)
{
  gmw_elwb_event_packet_t* event_pkt = (gmw_elwb_event_packet_t*) out_glossy_payload;
  switch (state) {
    case FIRST_SCHEDULE:
      /* if we have data, send event flag */
      if (!is_acked_by_host || data_len) {
        out_glossy_payload[0] = GMW_ELWB_EVENT_MAGIC_NUMBER;
        out_glossy_payload[1] = GMW_ELWB_EVENT_MAGIC_NUMBER;
        *out_len = 2;
        DEBUG_PRINT_INFO("Sent contention event");
      }
      break;
    case SECOND_SCHEDULE:
      /* empty */
      DEBUG_PRINT_WARNING("State second schedule should not have slots (pre)");
      break;
    case EVENT_ROUND:
      /* send how much data we have or if we are a new node */
      if(!is_acked_by_host) {
        if(is_contention_slot) {
          event_pkt->sender_id = node_id;
          event_pkt->n_data_pkts = 0;
          *out_len = sizeof(gmw_elwb_event_packet_t);
          DEBUG_PRINT_INFO("Sent event packet");
        }
      } else if (data_len && is_initiator) {
        n_data_packets = (data_len / GMW_ELWB_CONF_MAX_DATA_PKT_LEN) +
            ((data_len % GMW_ELWB_CONF_MAX_DATA_PKT_LEN) ? 1 : 0);
        transmit_counter = 0;
        event_pkt->sender_id = node_id;
        event_pkt->n_data_pkts = n_data_packets;
        *out_len = sizeof(gmw_elwb_event_packet_t);
        DEBUG_PRINT_INFO("Sent data request for %u packets",
                         event_pkt->n_data_pkts);
      }
      break;
    case DATA_ROUND:
      /* send data */
      if (is_initiator && (transmit_counter < n_data_packets)) {
        *out_len = MIN(GMW_ELWB_CONF_MAX_DATA_PKT_LEN, data_len);
        data_len -= *out_len;
        memcpy(out_glossy_payload,
            (data_buffer + transmit_counter * GMW_ELWB_CONF_MAX_DATA_PKT_LEN),
            *out_len);
        transmit_counter++;
        DEBUG_PRINT_INFO("Sent data packet with %u bytes", *out_len);
      }
      break;
    default:
      break;
  }
}
/*---------------------------------------------------------------------------*/
static void
on_slot_post(uint8_t slot_index, uint16_t slot_assignee, uint8_t len,
             uint8_t* glossy_payload, uint8_t is_initiator,
             uint8_t is_contention_slot)
{
  switch (state) {
    case FIRST_SCHEDULE:
      /* do nothing */
      break;
    case SECOND_SCHEDULE:
      /* empty */
      DEBUG_PRINT_WARNING("State second schedule should not have slots (post)");
      break;
    case EVENT_ROUND:
      /* send elwb event packet */
      break;
    case DATA_ROUND:
      /* receive ack */
      if(!is_acked_by_host) {
        if(HOST_ID == slot_assignee) {
          if(len == 2) {
            if(*((uint16_t*) glossy_payload) == node_id) {
              is_acked_by_host = 1;
              DEBUG_PRINT_INFO("Received ack!")
            }
          }
        }
      }
      break;
    default:
      break;
  }
}
/*---------------------------------------------------------------------------*/
static void
on_round_finish(gmw_pre_post_processes_t* in_out_pre_post_processes)
{
  if(current_control->schedule.period > 1000) {
    in_out_pre_post_processes->post_process_current_round = post_process;
    in_out_pre_post_processes->pre_process_next_round = pre_process;
  } else {
    in_out_pre_post_processes->post_process_current_round = NULL;
    in_out_pre_post_processes->pre_process_next_round = NULL;
  }
}
/*---------------------------------------------------------------------------*/
static uint32_t
on_bootstrap_timeout(void)
{
  /* we return to bootstrap immediately */
  return 0;
}
