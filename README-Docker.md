Docker container for MacOS
==========================

Prerequisites:
- Python: intelhex pyserial python-magic
`pip install intelhex pyserial python-magic`
- xQuartz is needed for cooja. (Might be preinstalled)
- Docker for Mac ;)

Create the Docker image (needs to be done only once)
`docker build tools/docker -t contiki`

In every new shell you need to do:
`source source_me_to_start`

This will enable the following commands in your shell. Use them from within your project:
- contiki_build: use like make. Will build inside the container. The output is put in the project folder as if make would run on MacOS
- contiki_install: Build and install on USB device. You need to specify the TARGET and the Makefile has to specify the CONTIKI_PROJECT
- contiki_cooja: run cooja.

TODO
----
- Check if cooja loads the flocklab plugin already
- start cooja with additional memory to make the flocklab plugin work
